from spacy.symbols import *
import spacy
nlp = spacy.load('fr', disable=['ner', 'textcat'])

np_labels = set(["nsubj", "nsubjpass", "obl", "obj"]) # Probably others too
def iter_nps(doc):
    for word in doc:
        print(word.text, word.dep_)
        if word.dep_ in np_labels:
            yield list(word.subtree)

sentence = u"""
Il y a plusieurs jours, Kanye West s'est rendu à la Maison Blanche pour rencontrer Donald Trump.
"""

doc = nlp(sentence)
print(list(iter_nps(doc)))