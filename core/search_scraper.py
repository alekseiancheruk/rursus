#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Aleksei Iancheruk

import sys
import time
import requests
from urllib.parse import urlparse
import pandas as pd
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.common.exceptions import NoSuchFrameException
from selenium.webdriver.common.keys import Keys

def start_browser():
    """
    Start browser instance
    """
    br = webdriver.Firefox()
    br.implicitly_wait(10)
    return br

def scrape_results(br):
    """
    Parse HTML page
    """
    pagelinks = []
    html = br.page_source
    soup = BeautifulSoup(html, 'html.parser')
    weblinks = soup.find_all('div', attrs={"class":"g"})
    for link in weblinks:
        url = link.find_all('a')[0]
        pagelinks.append(url.get('href'))

    return pagelinks

def go_to_page(br, page_num, search_term, lang, website=None):
    """
    Go to Google News Search page
    """
    page_num = page_num - 1
    start_results = page_num * 100
    start_results = str(start_results)
    if website is not None:
        url = f'https://www.google.com/search?num=100&start={start_results}&q={search_term}+site%3A{website}&source=lnt&tbs=lr:lang_1{lang}&lr=lang_{lang}'
        print('[*] Fetching 100 results from page '+str(page_num+1)+' at '+url)
        br.get(url)
        time.sleep(10)
    else:
        # For news
        #url = f'https://www.google.com/search?num=100&start={start_results}&q={search_term}&source=lnt&tbs=lr:lang_1{lang}&lr=lang_{lang}&source=lnms&tbm=nws'
        # For google main search
        url = f'https://www.google.com/search?num=100&start={start_results}&q={search_term}&source=lnt&tbs=lr:lang_1{lang}&lr=lang_{lang}'
        print('[*] Fetching 100 results from page '+str(page_num+1)+' at '+url)
        br.get(url)
        time.sleep(10)

def extract(search_term, lang, pages, website=None):
    """
    Extract article URL
    """
    br = start_browser()
    search_term = search_term.replace(' ', '+')

    all_results = []
    for page_num in range(int(pages)):
        page_num = page_num+1 # since it starts at 0
        go_to_page(br, page_num, search_term, lang, website)
        titles_urls = scrape_results(br)
        for title in titles_urls:
            all_results.append(title)
 
    br.quit()

    return all_results

def main():
    
    # One list - one topic
    # Topic + lang + number of results ("1" == "100")
    topics = [
        ["neural methods filetype:PDF", 'fr', '1'],
            ]

    all_texts = {"topic_id": [], "url": [], "source" : []}

    for id in range(len(topics)):
        urls = extract(topics[id][0], topics[id][1], topics[id][2])
        for url in urls:
            o = urlparse(url)
            all_texts["topic_id"].append(id)
            all_texts['url'].append(url)
            all_texts['source'].append(o.netloc)

    df = pd.DataFrame(data=all_texts)
    # Save to csv file
    df.to_csv('../data/srcs/srcs.tsv', sep='\t', encoding='utf-8')

if __name__ == '__main__':
    main()