import spacy
nlp = spacy.load('fr', disable=['ner', 'textcat'])


# SUBJECTS = ["nsubj", "nsubjpass"]  # add or delete more as you wish
# OBJECTS = ["dobj", "pobj", "dobj"]  # add or delete more as you wish

# sent = "A rebours de la plupart des rappeurs, Kanye West avait apporté son soutien à Donald Trump après son élection en novembre 2016, en lui rendant notamment visite à la Trump Tower de New York."

# doc = nlp(sent)
# sub_toks = [tok for tok in doc if (tok.dep_ in SUBJECTS)]
# obj_toks = [tok for tok in doc if (tok.dep_ in OBJECTS)]

# #print("Subjects:", sub_toks)
# #print("Objects :", obj_toks)

sentence = u"""
Donald Trump est le pire président des États-Unis, mais Hillary est meilleure que lui.
"""

doc2= nlp(sentence)
verbs = [tok for tok in doc2 if tok.pos_ == "VERB"]

for token in doc2:
    if token.dep_ == "ROOT":
        print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
            token.shape_, token.is_alpha, token.is_stop)

